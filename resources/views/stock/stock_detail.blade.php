<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Persediaan</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Persediaan
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/stock'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <h5>Detail Barang:</h5>
                    <h1>{{$stock->name}} ({{$stock->sku}})</h1>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>SKU</th>
                            <td>{{ $stock->sku }}</td>
                        </tr>
                        <tr>
                            <th>Nama Barang</th>
                            <td>{{ $stock->name }}</td>
                        </tr>
                        <tr>
                            <th>Deskripsi Barang</th>
                            <td>{{ $stock->description }}</td>
                        </tr>
                        <tr>
                            <th>Gambar</th>
                            <td><img src="{{ $stock->image_url }}" width="100px" /></td>
                        </tr>
                        <tr>
                            <th>Harga Beli</th>
                            <td>{{ $purc_price }}</td>
                        </tr>
                        <tr>
                            <th>Harga Jual</th>
                            <td>{{ $sell_price }}</td>
                        </tr>
                        <tr>
                            <th>Jumlah Stok</th>
                            <td>{{ $stock->qty }}</td>
                        </tr>
                        <tr>
                            <th>Satuan Barang</th>
                            <td>{{ $stock->type }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="get" action="{!! url('/stock/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $stock->id }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/stock/destroy'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $stock->id }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>