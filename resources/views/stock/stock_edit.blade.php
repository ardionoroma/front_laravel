<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Persediaan</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Persediaan - <strong>EDIT DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/stock'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form method="post" action="{!! url('/stock/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Barang .." value="{{$stock->name}}">
                            @if($errors->has('name'))
                                <div class="text-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Deskripsi Barang</label>
                            <input type="text" name="description" class="form-control" placeholder="Deskripsi Barang .." value="{{$stock->description}}">
                             @if($errors->has('description'))
                                <div class="text-danger">
                                    {{ $errors->first('description')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>URL Gambar Barang</label>
                            <input type="text" name="image_url" class="form-control" placeholder="URL Gambar Barang .." value="{{$stock->image_url}}">
                            @if($errors->has('image_url'))
                                <div class="text-danger">
                                    {{ $errors->first('image_url')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Harga Beli (Rupiah)</label>
                            <input type="text" name="purc_price" class="form-control" placeholder="Harga Beli (Rupiah) .." value="{{$stock->purc_price}}">
                             @if($errors->has('purc_price'))
                                <div class="text-danger">
                                    {{ $errors->first('purc_price')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Harga Jual (Rupiah)</label>
                            <input type="text" name="sell_price" class="form-control" placeholder="Harga Jual (Rupiah) .." value="{{$stock->sell_price}}">
                            @if($errors->has('sell_price'))
                                <div class="text-danger">
                                    {{ $errors->first('sell_price')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Jumlah Barang</label>
                            <input type="text" name="qty" class="form-control" placeholder="Jumlah Barang .." value="{{$stock->qty}}">
                            @if($errors->has('qty'))
                                <div class="text-danger">
                                    {{ $errors->first('qty')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Jenis Satuan</label>
                            <select name="type" class="form-control">
                                <option value="pcs">Pcs</option>
                                <option value="box">Box</option>
                                <option value="lusin">Lusin</option>
                                <option value="dus">Dus</option>
                                <option value="ton">Ton</option>
                                <option value="kg">Kilogram</option>
                                <option value="ltr">Liter</option>
                                <option value="m">Meter</option>
                                <option value="unit">Unit</option>
                                <option value="gr">Gram</option>
                                <option value="cm">Sentimeter</option>
                                <option value="order">Order</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="id" value="{{ $stock->id }}">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>