<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TemanBisnis</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                /*font-size: 13px;*/
                font-size: 16px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                /*text-transform: uppercase;*/
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
        <?php if(empty($token)) { ?>
            <div class="top-right links">
                <a href="{!! url('/login'); !!}">Masuk</a>
                <a href="{!! url('/register'); !!}">Daftar</a>
            </div>
        <?php } else { ?>
            <div class="top-right links">
                <a href="{!! url('/user'); !!}">Profil Usaha</a>
                <a href="{!! url('/logout'); !!}">Keluar</a>
            </div>
        <?php } ?>
            

            <div class="content">
                <div class="title m-b-md">
                    <img src="http://temanbisnisapp.com/imgTemplateEmailMarketing/logo-06.png" width="300px"> Web <img src="http://temanbisnisapp.com/imgTemplateEmailMarketing/tebidompet.jpeg" width="100px">
                </div>

                <?php if(!empty($token)) { ?>
                <div class="links">
                    <a href="{!! url('/transaction'); !!}">Transaksi</a>
                    <a href="{!! url('/sales'); !!}">Transaksi Barang</a>
                    <a href="{!! url('/stock'); !!}">Persediaan</a>
                    <a href="{!! url('/cs'); !!}">Kontak Bisnis</a>
                    <a href="{!! url('/categorytrx'); !!}">Kategori</a>
                    <!-- <a href="{!! url('/note'); !!}">Catatan</a> -->
                </div>
                <?php } ?>
            </div>
        </div>
    </body>
</html>
