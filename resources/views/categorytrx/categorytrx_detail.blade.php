<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Kategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Kategori
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/categorytrx'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h1>Detail Kategori</h1>
                    <h3>{{$categorytrx->title}}</h3>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>Nama Kategori</th>
                            <td>{{ $categorytrx->title }}</td>
                        </tr>
                        <tr>
                            <th>Ikon</th>
                            <td><img src="{{ $categorytrx->image_url }}" width="100px" /></td>
                        </tr>
                        <tr>
                            <th>Tipe Pembayaran</th>
                            <?php
                                if ($categorytrx->allow_non_cash) {
                                    echo "<td>Non Cash</td>";
                                } else {
                                    echo "<td>Cash</td>";
                                }
                            ?>
                        </tr>
                        <tr>
                            <th>Satuan</th>
                            <?php
                                if ($categorytrx->allow_unit) {
                                    echo "<td>Unit</td>";
                                } else {
                                    echo "<td>Non Unit</td>";
                                }
                            ?>
                        </tr>
                        <tr>
                            <th>Jenis Transaksi</th>
                            <td>{{ $categorytrx->type_of_trx }}</td>
                        </tr>
                        <tr>
                            <th>Terakhir Digunakan</th>
                            <td>{{ $categorytrx->last_usage }}</td>
                        </tr>
                        <tr>
                        <?php
                            if ($categorytrx->editable) { 
                        ?>
                            <td> 
                                <form method="get" action="{!! url('/categorytrx/edit'); !!}" style=">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $categorytrx->id }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right;">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/categorytrx/destroy'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $categorytrx->id }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left;">
                                </form>  
                            </td>
                        <?php 
                            } else { 
                        ?>
                            <td style="text-align: center;" colspan="2">Tidak dapat mengubah kategori bawaan.</td>
                        <?php
                            }
                        ?>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>