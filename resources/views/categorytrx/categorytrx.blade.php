<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Kategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <a href="{!! url('/'); !!}" style="float: left;"><h6>Beranda</h6></a>
                    <h6 style="float: right;">CRUD Data Kategori</h6>
                </div>
                <div class="card-body">
                    <div>
                        <a href="{!! url('/categorytrx/add'); !!}" class="btn btn-primary">Input Kategori Baru</a>
                        <form class="form-inline" method="get" action="{!! url('/categorytrx/find'); !!}" style="float: right;">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="cari" class="form-control" placeholder="Cari ..">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Cari">
                            </div>
                        </form>
                    </div>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Kategori</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorytrx as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>
                                    <form action="{!! url('/categorytrx/detail'); !!}" method="get">
                                        {{ csrf_field() }}
                                        <button type="submit" name="id" value="{{ $p->id }}" class="btn-link">{{ $p->title }}</button>
                                    </form>
                                </td>
                                <td>
                                <?php
                                    if ($p->editable) { 
                                ?>   
                                    <form method="get" action="{!! url('/categorytrx/edit'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/categorytrx/destroy'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>  
                                <?php 
                                    } else { 
                                ?>
                                    <p>Tidak dapat mengubah kategori bawaan.</p>
                                <?php
                                    }
                                ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>