<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Kategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Kategori - <strong>EDIT DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/categorytrx'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form method="post" action="{!! url('/categorytrx/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" name="title" class="form-control" placeholder="Nama Kategori .." value="{{ $categorytrx->title }}">
                            @if($errors->has('title'))
                                <div class="text-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Link Gambar</label>
                            <input type="text" name="image_url" class="form-control" placeholder="Link Gambar .." value="{{ $categorytrx->image_url }}">
                             @if($errors->has('image_url'))
                                <div class="text-danger">
                                    {{ $errors->first('image_url')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tipe Pembayaran</label>
                            <select name="allow_non_cash" class="form-control">
                                <option value="1">Non Tunai</option>
                                <option value="0">Tunai</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <select name="allow_unit" class="form-control">
                                <option value="1">Unit</option>
                                <option value="0">Non Unit</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="id" value="{{ $categorytrx->id }}">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>