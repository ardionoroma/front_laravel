<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Kategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Kategori
                </div>
                <div class="card-body">
                    <h3>Hasil pencarian berdasarkan {{ $source }}</h3>
                    <a href="{!! url('/categorytrx'); !!}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <?php
                        if ($source == 'N/A' || empty($categorytrx)) {
                    ?>
                    <br>
                    <br>
                    <br>
                    <h5 style="text-align: center;">Tidak ada data yang sesuai dengan pencarian.</h5>
                    <?php
                        } else {
                    ?>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Kategori</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categorytrx as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>
                                    <form action="{!! url('/categorytrx/detail'); !!}" method="get">
                                        {{ csrf_field() }}
                                        <button type="submit" name="id" value="{{ $p->id }}" class="btn-link">{{ $p->title }}</button>
                                    </form>
                                </td>
                                <td>
                                <?php
                                    if ($p->editable) { 
                                ?>   
                                    <form method="get" action="{!! url('/categorytrx/edit'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/categorytrx/destroy'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>  
                                <?php 
                                    } else { 
                                ?>
                                    <p>Tidak dapat mengubah kategori bawaan.</p>
                                <?php
                                    }
                                ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>