<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Info Pengguna</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    TemanBisnis - Info Pengguna
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/'); !!}" class="btn btn-primary" style="float: left">Kembali ke Beranda</a>
                    <br/>
                    <br/>
                    <br/>
                    <h5>Detail Pengguna</h5>
                    <h1>{{$user->business_name}}</h1>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>UUID Pengguna</th>
                            <td>{{ $user->user_uuid }}</td>
                        </tr>
                        <tr>
                            <th>Nama Pemilik</th>
                            <td>{{ $user->owner_name }}</td>
                        </tr>
                        <tr>
                            <th>Kategori Usaha</th>
                            <td>{{ $user->business_category }}</td>
                        </tr>
                        <tr>
                            <th>Model Usaha</th>
                            <td>{{ $user->business_model }}</td>
                        </tr>
                        <tr>
                            <th>Produk Unggulan</th>
                            <td>{{ $user->featured_product }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $user->address }}</td>
                        </tr>
                        <tr>
                            <th>Kota</th>
                            <td>{{ $user->city }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Telepon</th>
                            <td>{{ $user->phone }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Rekening</th>
                            <td>{{ $user->bank_account }}</td>
                        </tr>
                        <tr>
                            <th>Website</th>
                            <td>{{ $user->website }}</td>
                        </tr>
                        <tr>
                            <th>Website Lain</th>
                            <td>{{ $user->other_website }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <form method="get" action="{!! url('/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="submit" class="btn btn-warning" value="Ubah Info Pengguna">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>