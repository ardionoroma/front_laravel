<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Masuk</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    TemanBisnis - <strong>MASUK</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/'); !!}" class="btn btn-primary">Kembali</a>
                    <?php if(!empty($invalid_credentials)) { ?>
                        <h5 style="color: red; text-align: center;">Email atau password yang Anda masukkan salah.</h5>
                    <?php } ?>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/auth'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Email ..">
                            @if($errors->has('email'))
                                <div class="text-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Password ..">
                             @if($errors->has('password'))
                                <div class="text-danger">
                                    {{ $errors->first('password')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Masuk">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>