<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Daftar</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    TemanBisnis - <strong>UBAH INFO</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/user'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Nama Usaha</label>
                            <input type="text" name="business_name" class="form-control" placeholder="Nama Usaha .." value="{{ $user->business_name }}">
                            @if($errors->has('business_name'))
                                <div class="text-danger">
                                    {{ $errors->first('business_name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Model Usaha</label>
                            <select name="business_model" class="form-control">
                                <option value="Produksi">Produksi</option>
                                <option value="Retail">Retail</option>
                                <option value="Jasa">Jasa</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Kategori Usaha</label>
                            <input type="text" name="business_category" class="form-control" placeholder="Kategori Usaha .." value="{{ $user->business_category }}">
                            @if($errors->has('business_category'))
                                <div class="text-danger">
                                    {{ $errors->first('business_category')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kota</label>
                            <input type="text" name="city" class="form-control" placeholder="Kota .." value="{{ $user->city }}">
                            @if($errors->has('city'))
                                <div class="text-danger">
                                    {{ $errors->first('city')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Produk Unggulan</label>
                            <input type="text" name="featured_product" class="form-control" placeholder="Produk Unggulan .." value="{{ $user->featured_product }}">
                            @if($errors->has('featured_product'))
                                <div class="text-danger">
                                    {{ $errors->first('featured_product')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nama Pemilik Usaha</label>
                            <input type="text" name="owner_name" class="form-control" placeholder="Nama Pemilik Usaha .." value="{{ $user->owner_name }}">
                            @if($errors->has('owner_name'))
                                <div class="text-danger">
                                    {{ $errors->first('owner_name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>No. Telepon</label>
                            <input type="text" name="phone" class="form-control" placeholder="No. Telepon .." value="{{ $user->phone }}">
                            @if($errors->has('phone'))
                                <div class="text-danger">
                                    {{ $errors->first('phone')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>No. Rekening</label>
                            <input type="text" name="bank_account" class="form-control" placeholder="No. Rekening .." value="{{ $user->bank_account }}">
                            @if($errors->has('bank_account'))
                                <div class="text-danger">
                                    {{ $errors->first('bank_account')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="address" class="form-control" placeholder="Alamat .." value="{{ $user->address }}">
                             @if($errors->has('address'))
                                <div class="text-danger">
                                    {{ $errors->first('address')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Website</label>
                            <input type="text" name="website" class="form-control" placeholder="Website .." value="{{ $user->website }}">
                            @if($errors->has('website'))
                                <div class="text-danger">
                                    {{ $errors->first('website')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Website Lain</label>
                            <input type="text" name="other_website" class="form-control" placeholder="Website Lain .." value="{{ $user->other_website }}">
                            @if($errors->has('other_website'))
                                <div class="text-danger">
                                    {{ $errors->first('other_website')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>