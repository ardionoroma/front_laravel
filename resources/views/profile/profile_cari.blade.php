<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Pengguna</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Pengguna
                </div>
                <div class="card-body">
                    <h3>Hasil pencarian berdasarkan {{ $source }}</h3>
                    <a href="{!! url('/profile'); !!}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 85%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>ID Pengguna</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- Using if because query by Piutang ID results singular. It causes error if using foreach to generate the record. -->
                        <?php
                            if ($source != "ID Pengguna") {
                        ?>
                            @foreach($profile as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>
                                    <form action="{!! url('/profile/detail'); !!}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" name="detail" value="{{ $p->uuid }}" class="btn-link">{{ $p->owner_name }}</button>
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{!! url('/profile/edit'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/profile/hapus'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        <?php } else { ?>
                            <tr>
                                <td>1.</td>
                                <td>
                                    <form action="{!! url('/profile/detail'); !!}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" name="detail" value="{{ $profile->uuid }}" class="btn-link">{{ $profile->owner_name }}</button>
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{!! url('/profile/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $profile->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/profile/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $profile->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>