<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Pengguna</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Pengguna
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/profile'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <h5>Detail Pengguna</h5>
                    <h1>{{$profile->owner_name}}</h1>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>ID Pengguna</th>
                            <td>{{ $profile->uuid }}</td>
                        </tr>
                        <tr>
                            <th>Nama Pengguna</th>
                            <td>{{ $profile->owner_name }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $profile->email }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Telepon</th>
                            <td>{{ $profile->phone }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $profile->address }}</td>
                        </tr>
                        <tr>
                            <th>Kota</th>
                            <td>{{ $profile->city }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Rekening</th>
                            <td>{{ $profile->bank_account }}</td>
                        </tr>
                        <tr>
                            <th>Nama Usaha</th>
                            <td>{{ $profile->business_name }}</td>
                        </tr>
                        <tr>
                            <th>Kategori Usaha</th>
                            <td>{{ $profile->business_category }}</td>
                        </tr>
                        <tr>
                            <th>Model Usaha</th>
                            <td>{{ $profile->business_model }}</td>
                        </tr>
                        <tr>
                            <th>Produk Unggulan</th>
                            <td>{{ $profile->featured_product }}</td>
                        </tr>
                        <tr>
                            <th>Token FCM</th>
                            <td>{{ $profile->fcm_token }}</td>
                        </tr>
                        <tr>
                            <th>Foto Profil</th>
                            <td><img src="{{ $profile->image_profile }}" width="100px"></td>
                        </tr>
                        <tr>
                            <th>Situs</th>
                            <td>{{ $profile->website }}</td>
                        </tr>
                        <tr>
                            <th>Situs Lain</th>
                            <td>{{ $profile->other_website }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="post" action="{!! url('/profile/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $profile->uuid }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/profile/hapus'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $profile->uuid }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>