<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Pengguna</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Pengguna - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/profile'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/profile/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama Pengguna</label>
                            <input type="text" name="owner_name" class="form-control" placeholder="Nama Pengguna ..">
                            @if($errors->has('owner_name'))
                                <div class="text-danger">
                                    {{ $errors->first('owner_name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Email ..">
                            @if($errors->has('email'))
                                <div class="text-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nomor Telepon</label>
                            <input type="text" name="phone" class="form-control" placeholder="Nomor Telepon ..">
                             @if($errors->has('phone'))
                                <div class="text-danger">
                                    {{ $errors->first('phone')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="address" class="form-control" placeholder="Alamat ..">
                            @if($errors->has('address'))
                                <div class="text-danger">
                                    {{ $errors->first('address')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kota</label>
                            <input type="text" name="city" class="form-control" placeholder="Kota ..">
                            @if($errors->has('city'))
                                <div class="text-danger">
                                    {{ $errors->first('city')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nomor Rekening</label>
                            <input type="text" name="bank_account" class="form-control" placeholder="Nomor Rekening ..">
                            @if($errors->has('bank_account'))
                                <div class="text-danger">
                                    {{ $errors->first('bank_account')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nama Usaha</label>
                            <input type="text" name="business_name" class="form-control" placeholder="Nama Usaha ..">
                            @if($errors->has('business_name'))
                                <div class="text-danger">
                                    {{ $errors->first('business_name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kategori Usaha</label>
                            <input type="text" name="business_category" class="form-control" placeholder="Kategori Usaha ..">
                            @if($errors->has('business_category'))
                                <div class="text-danger">
                                    {{ $errors->first('business_category')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Model Usaha</label>
                            <select name="business_model" class="form-control">
                                <option value="Produksi">Produksi</option>
                                <option value="Retail">Retail</option>
                                <option value="Jasa">Jasa</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Produk Unggulan</label>
                            <input type="text" name="featured_product" class="form-control" placeholder="Produk Unggulan ..">
                            @if($errors->has('featured_product'))
                                <div class="text-danger">
                                    {{ $errors->first('featured_product')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Token FCM</label>
                            <input type="text" name="fcm_token" class="form-control" placeholder="Token FCM ..">
                            @if($errors->has('fcm_token'))
                                <div class="text-danger">
                                    {{ $errors->first('fcm_token')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Foto Profil</label>
                            <input type="text" name="image_profile" class="form-control" placeholder="Foto Profil ..">
                            @if($errors->has('image_profile'))
                                <div class="text-danger">
                                    {{ $errors->first('image_profile')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Situs</label>
                            <input type="text" name="profile_uuid" class="form-control" placeholder="Situs ..">
                             @if($errors->has('profile_uuid'))
                                <div class="text-danger">
                                    {{ $errors->first('website')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Situs Lain</label>
                            <input type="text" name="other_website" class="form-control" placeholder="Situs Lain ..">
                            @if($errors->has('other_website'))
                                <div class="text-danger">
                                    {{ $errors->first('other_website')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Fulfilled</label>
                            <select name="fulfilled" class="form-control">
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>