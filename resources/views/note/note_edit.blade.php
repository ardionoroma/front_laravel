<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Catatan</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Catatan - <strong>EDIT DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/note'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form method="post" action="{!! url('/note/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Judul</label>
                            <input type="text" name="title" class="form-control" placeholder="Judul .." value="{{ $note->title }}">
                            @if($errors->has('title'))
                                <div class="text-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Isi</label>
                            <textarea name="content" class="form-control" placeholder="Isi Catatan .." rows="5">{{ $note->content }}</textarea>
                             @if($errors->has('content'))
                                <div class="text-danger">
                                    {{ $errors->first('content')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="uuid" value="{{ $note->uuid }}">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>