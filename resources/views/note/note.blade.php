<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Catatan</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <a href="{!! url('/'); !!}" style="float: left;"><h6>Beranda</h6></a>
                    <h6 style="float: right;">CRUD Data Catatan</h6>
                </div>
                <div class="card-body">
                    <div>
                        <a href="{!! url('/note/tambah'); !!}" class="btn btn-primary">Input Catatan Baru</a>
                        <form class="form-inline" method="post" action="{!! url('/note/search'); !!}" style="float: right;">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="search" class="form-control" placeholder="Cari ..">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Cari">
                            </div>
                        </form>
                    </div>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>ID Catatan</th>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>ID Pengguna</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($note as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>{{ $p->uuid }}</td>
                                <td>{{ $p->title }}</td>
                                <td>{{ $p->content }}</td>
                                <td>{{ $p->profile_uuid }}</td>
                                <td>
                                    <form method="post" action="{!! url('/note/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/note/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>