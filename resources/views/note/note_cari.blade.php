<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Catatan</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Catatan
                </div>
                <div class="card-body">
                    <h3>Hasil pencarian berdasarkan {{ $source }}</h3>
                    <a href="{!! url('/note'); !!}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>ID Catatan</th>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>ID Pengguna</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- Using if because query by Transaction ID results singular. It causes error if using foreach to generate the record. -->
                        <?php
                            if ($source != "ID Catatan") {
                        ?>
                            @foreach($note as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>{{ $p->uuid }}</td>
                                <td>{{ $p->title }}</td>
                                <td>{{ $p->content }}</td>
                                <td>{{ $p->profile_uuid }}</td>
                                <td>
                                    <form method="post" action="{!! url('/note/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/note/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        <?php } else { ?>
                            <tr>
                                <td>1.</td>
                                <td>{{ $note->uuid }}</td>
                                <td>{{ $note->title }}</td>
                                <td>{{ $note->content }}</td>
                                <td>{{ $note->profile_uuid }}</td>
                                <td>
                                    <form method="post" action="{!! url('/note/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $note->uuid }}">
                                        <input type="submit" class="btn btn-warning" valunotee="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/note/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $note->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>