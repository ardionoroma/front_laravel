<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Transaksi</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Transaksi
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/transaction'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h1>Detail Transaksi</h1>
                    <h3>ID: {{$transaction->transaction_uuid}}</h3>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>Nominal</th>
                            <td>{{ $nominal }}</td>
                        </tr>
                        <tr>
                            <th>Note</th>
                            <td>{{ $transaction->note }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal</th>
                            <td><?php echo \Date::parse($transaction->date)->format("l, j F Y"); ?></td>
                        </tr>
                        <tr>
                            <th>Nama Kontak Bisnis</th>
                            <td>{{ $transaction->nama_cs }}</td>
                        </tr>
                        <tr>
                            <th>Nama Kategori</th>
                            <td>{{ $transaction->nama_kategori }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="get" action="{!! url('/transaction/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $transaction->id }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/transaction/destroy'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $transaction->id }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>