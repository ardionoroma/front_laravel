<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Kontak Bisnis</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Kontak Bisnis - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/cs'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/cs/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama ..">
                            @if($errors->has('name'))
                                <div class="text-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name="address" class="form-control" placeholder="Alamat ..">
                             @if($errors->has('address'))
                                <div class="text-danger">
                                    {{ $errors->first('address')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>No. Telepon</label>
                            <input type="text" name="phone" class="form-control" placeholder="No. Telepon ..">
                            @if($errors->has('phone'))
                                <div class="text-danger">
                                    {{ $errors->first('phone')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Email ..">
                             @if($errors->has('email'))
                                <div class="text-danger">
                                    {{ $errors->first('email')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>No. Rekening</label>
                            <input type="text" name="bank_account" class="form-control" placeholder="No. Rekening ..">
                            @if($errors->has('bank_account'))
                                <div class="text-danger">
                                    {{ $errors->first('bank_account')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="category" class="form-control">
                                <option value="Customer">Customer</option>
                                <option value="Supplier">Supplier</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>