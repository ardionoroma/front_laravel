<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Kontak Bisnis</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Kontak Bisnis
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/cs'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <h5>Detail {{ $cs->category }}</h5>
                    <h1>{{$cs->name}}</h1>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>Nama {{ $cs->category }}</th>
                            <td>{{ $cs->name }}</td>
                        </tr>
                        <tr>
                            <th>Alamat</th>
                            <td>{{ $cs->address }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Telepon</th>
                            <td>{{ $cs->phone }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{ $cs->email }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Rekening</th>
                            <td>{{ $cs->bank_account }}</td>
                        </tr>
                        <tr>
                            <th>Kategori</th>
                            <td>{{ $cs->category }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="get" action="{!! url('/cs/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $cs->id }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/cs/destroy'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $cs->id }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>