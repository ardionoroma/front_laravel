<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Transaksi Barang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Transaksi Barang
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/sales'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h1>Detail Transaksi</h1>
                    <h3>ID: {{$sales->id}}</h3>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>Tanggal</th>
                            <td>{{ $sales->date }}</td>
                        </tr>
                        <tr>
                            <th>SKU</th>
                            <td>{{ $sales->sku }}</td>
                        </tr>
                        <tr>
                            <th>Nama Barang</th>
                            <td>{{ $sales->name }}</td>
                        </tr>
                        <tr>
                            <th>Jumlah</th>
                            <?php if($sales->qty < 0) { ?>
                                <td>{{ $sales->qty * -1 }} {{ $sales->type }}.</td>
                            <?php } else { ?>
                                <td>{{ $sales->qty }} {{ $sales->type }}.</td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <?php if($sales->init == 1) { ?>
                                <td>Persediaan pertama.</td>
                            <?php } else if ($sales->init == 2) { ?>
                                <td>Penyesuaian.</td>
                            <?php } else { ?>
                                <?php if($sales->qty < 0) { ?>
                                    <td>Terjual sebanyak {{ $sales->qty * -1 }} {{ $sales->type }}.</td>
                                <?php } else { ?>
                                    <td>Membeli persediaan sebanyak {{ $sales->qty }} {{ $sales->type }}.</td>
                                <?php } 
                            } ?>
                        </tr>
                        <tr>
                            <th>Nominal Transaksi</th>
                            <td>{{ $price }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="get" action="{!! url('/sales/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $sales->id }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/sales/destroy'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $sales->id }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>