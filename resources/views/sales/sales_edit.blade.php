<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#date").datepicker({
                    changeMonth:true,
                    changeYear:true,
                    yearRange:"-10:+0",
                    dateFormat:"yy-mm-dd"
                });
            });
       </script>
        <title>TemanBisnis - Transaksi Barang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Transaksi Barang - <strong>EDIT DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/sales'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form method="post" action="{!! url('/sales/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name="date" id="date" class="form-control" placeholder="Tanggal Transaksi .." value="{{ $sales->date }}">
                            @if($errors->has('date'))
                                <div class="text-danger">
                                    {{ $errors->first('date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Jumlah Barang</label>
                            <input type="hidden" name="qty_before" id="qty_before" value="{{ $sales->qty }}">
                            <?php if($sales->qty < 0) { ?>
                                <input type="text" name="qty" class="form-control" placeholder="Jumlah Barang .." value="{{ $sales->qty * -1 }}">
                            <?php } else { ?>
                                <input type="text" name="qty" class="form-control" placeholder="Jumlah Barang .." value="{{ $sales->qty }}">
                            <?php } ?>
                             @if($errors->has('qty'))
                                <div class="text-danger">
                                    {{ $errors->first('qty')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Catatan</label>
                            <input type="text" name="note" class="form-control" placeholder="Catatan Transaksi ..">
                            @if($errors->has('note'))
                                <div class="text-danger">
                                    {{ $errors->first('note')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nama Customer/Supplier</label>
                            <select id="select_cs" name="select_cs" class="form-control">
                                <option value="">-- N/A --</option>
                                @foreach($cs as $key=>$cus)
                                    <option value="{{ $cus->id }}">{{ $cus->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="cs_id" id="cs_id">
                            @if($errors->has('cs_id'))
                                <div class="text-danger">
                                    {{ $errors->first('cs_id')}}
                                </div>
                            @endif
                            <p>Last selected: <code id="last-cs"></code></p>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="id" value="{{ $sales->id }}">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#select_cs')
                .editableSelect()
                .on('select.editable-select', function (e, li){
                    $('#cs_id').val(li.attr('value')); 
                    li.value = li.attr('value')
                    $('#last-cs').html(
                        li.attr('value') + '. ' + li.text() + " | " + li.val() + '. ' + li.text() 
                    );
                });
        </script>
    </body>
</html>