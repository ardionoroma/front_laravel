<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Transaksi Barang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Transaksi Barang
                </div>
                <div class="card-body">
                    <h3>Hasil pencarian berdasarkan {{ $source }}</h3>
                    <a href="{!! url('/sales'); !!}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <?php
                        if ($source == 'N/A' || empty($sales)) {
                    ?>
                    <br>
                    <br>
                    <br>
                    <h5 style="text-align: center;">Tidak ada data yang sesuai dengan pencarian.</h5>
                    <?php
                        } else {
                    ?>
                    <table class="table table-bordered" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <?php
                        for ($i=0; $i < count($sales); $i++) { ?>
                        <tr style="text-align:left;background-color:rgba(0,0,0,.05)">
                            <td colspan="2">
                                <?php echo \Date::parse($sales[$i]->date)->format("l, j M Y"); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Transaksi</th>
                        </tr>
                            <?php for ($j=0; $j < count($sales[$i]->content); $j++) { ?>
                            <tr>
                                <td>{{ ++$count }}.</td>
                                <td>
                                    <form action="{!! url('/sales/detail'); !!}" method="get">
                                        {{ csrf_field() }}
                                        <button type="submit" name="id" value="{{ $sales[$i]->content[$j]->id }}" class="btn-link">{{ $sales[$i]->content[$j]->sku }}</button>
                                    </form>
                                </td>
                            </tr>
                            <?php }
                            $count = 0;
                        }
                        ?>
                    </table>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>