<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Transaksi Barang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header">
                    <a href="{!! url('/'); !!}" style="float: left;"><h6>Beranda</h6></a>
                    <h6 style="float: right;">CRUD Data Transaksi Barang</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{!! url('/sales/add'); !!}" class="btn btn-primary">Input Transaksi Barang Baru</a>
                        </div>
                        
                        <div class="col-sm-6">
                            <form class="form-inline" method="get" action="{!! url('/sales/find'); !!}" style="float: right;">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input type="text" name="cari" class="form-control" placeholder="Cari ..">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" value="Cari">
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <table class="table table-bordered" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <?php
                        for ($i=0; $i < count($sales); $i++) { ?>
                        <tr style="text-align:left;background-color:rgba(0,0,0,.05)">
                            <td colspan="2">
                                <?php echo \Date::parse($sales[$i]->date)->format("l, j M Y"); ?>
                            </td>
                        </tr>
                        <tr>
                            <th>No.</th>
                            <th>Transaksi</th>
                        </tr>
                            <?php for ($j=0; $j < count($sales[$i]->content); $j++) { ?>
                            <tr>
                                <td>{{ ++$count }}.</td>
                                <td>
                                    <form action="{!! url('/sales/detail'); !!}" method="get">
                                        {{ csrf_field() }}
                                        <button type="submit" name="id" value="{{ $sales[$i]->content[$j]->id }}" class="btn-link">{{ $sales[$i]->content[$j]->sku }}</button>
                                    </form>
                                </td>
                            </tr>
                            <?php }
                            $count = 0;
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>