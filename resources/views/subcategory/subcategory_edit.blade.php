<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Subkategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Subkategori - <strong>EDIT DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/subcategory'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    <form method="post" action="{!! url('/subcategory/update'); !!}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Nama Subkategori</label>
                            <input type="text" name="title" class="form-control" placeholder="Nama Subkategori .." value="{{ $subcategory->title }}">
                            @if($errors->has('title'))
                                <div class="text-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Link Gambar</label>
                            <input type="text" name="image_url" class="form-control" placeholder="Link Gambar .." value="{{ $subcategory->image_url }}">
                             @if($errors->has('image_url'))
                                <div class="text-danger">
                                    {{ $errors->first('image_url')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tipe Pembayaran</label>
                            <select name="allow_non_cash" class="form-control">
                                <option value="1">Non Cash</option>
                                <option value="0">Cash</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <select name="allow_unit" class="form-control">
                                <option value="1">Unit</option>
                                <option value="0">Non Unit</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="uuid" value="{{ $subcategory->uuid }}">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>