<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Subkategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Subkategori - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/subcategory'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/subcategory/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama Subkategori</label>
                            <input type="text" name="title" class="form-control" placeholder="Nama Subkategori ..">
                            @if($errors->has('title'))
                                <div class="text-danger">
                                    {{ $errors->first('title')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Link Gambar</label>
                            <input type="text" name="image_url" class="form-control" placeholder="Link Gambar ..">
                             @if($errors->has('image_url'))
                                <div class="text-danger">
                                    {{ $errors->first('image_url')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tipe Pembayaran</label>
                            <select name="allow_non_cash" class="form-control">
                                <option value="1">Non Cash</option>
                                <option value="0">Cash</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Satuan</label>
                            <select name="allow_unit" class="form-control">
                                <option value="1">Unit</option>
                                <option value="0">Non Unit</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Jenis Transaksi</label>
                            <select name="type_of_trx" class="form-control">
                                <option value="Income">Income</option>
                                <option value="Outcome">Outcome</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>ID Kategori</label>
                            <input type="text" name="categorytrx_uuid" class="form-control" placeholder="ID Kategori ..">
                            @if($errors->has('categorytrx_uuid'))
                                <div class="text-danger">
                                    {{ $errors->first('categorytrx_uuid')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>