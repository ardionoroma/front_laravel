<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Subkategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Subkategori
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/subcategory'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h1>Detail Subkategori</h1>
                    <h3>{{$subcategory->title}}</h3>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>ID Kategori</th>
                            <td>{{ $subcategory->uuid }}</td>
                        </tr>
                        <tr>
                            <th>Nama Kategori</th>
                            <td>{{ $subcategory->title }}</td>
                        </tr>
                        <tr>
                            <th>Ikon</th>
                            <td><img src="{{ $subcategory->image_url }}" width="100px" /></td>
                        </tr>
                        <tr>
                            <th>Tipe Pembayaran</th>
                            <?php
                                if ($subcategory->allow_non_cash) {
                                    echo "<td>Non Cash</td>";
                                } else {
                                    echo "<td>Cash</td>";
                                }
                            ?>
                        </tr>
                        <tr>
                            <th>Satuan</th>
                            <?php
                                if ($subcategory->allow_unit) {
                                    echo "<td>Unit</td>";
                                } else {
                                    echo "<td>Non Unit</td>";
                                }
                            ?>
                        </tr>
                        <tr>
                            <th>Jenis Transaksi</th>
                            <td>{{ $subcategory->type_of_trx }}</td>
                        </tr>
                        <tr>
                            <th>Terakhir Digunakan</th>
                            <td>{{ $subcategory->last_usage }}</td>
                        </tr>
                        <tr>
                            <th>ID Kategori</th>
                            <td>{{ $subcategory->categorytrx_uuid }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="post" action="{!! url('/subcategory/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $subcategory->uuid }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/subcategory/hapus'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $subcategory->uuid }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>