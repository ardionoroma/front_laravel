<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Subkategori</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Subkategori
                </div>
                <div class="card-body">
                    <h3>Hasil pencarian berdasarkan {{ $source }}</h3>
                    <a href="{!! url('/subcategory'); !!}" class="btn btn-primary">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>ID Subkategori</th>
                                <th>Nama Subkategori</th>
                                <th style="width: 125px">Ikon</th>
                                <th>Tipe Pembayaran</th>
                                <th>Satuan</th>
                                <th>Jenis Transaksi</th>
                                <th>ID Kategori</th>
                                <th>Terakhir Digunakan</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                        <!-- Using if because query by Transaction ID results singular. It causes error if using foreach to generate the record. -->
                        <?php
                            if ($source != "ID Subkategori") {
                        ?>
                            @foreach($subcategory as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>{{ $p->uuid }}</td>
                                <td>{{ $p->title }}</td>
                                <td><img src="{{ $p->image_url }}" width="100px"></td>
                                <?php
                                if ($p->allow_non_cash) {
                                    echo "<td>Non Cash</td>";
                                } else {
                                    echo "<td>Cash</td>";
                                }

                                if ($p->allow_unit) {
                                    echo "<td>Unit</td>";
                                } else {
                                    echo "<td>Non Unit</td>";
                                }
                                ?>
                                <td>{{ $p->type_of_trx }}</td>
                                <td>{{ $p->categorytrx_uuid }}</td>
                                <td>{{ $p->last_usage }}</td>
                                <td>
                                    <form method="post" action="{!! url('/subcategory/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/subcategory/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        <?php } else { ?>
                            <tr>
                                <td>1.</td>
                                <td>{{ $subcategory->uuid }}</td>
                                <td>{{ $subcategory->title }}</td>
                                <td><img src="{{ $subcategory->image_url }}" width="100px"></td>
                                <?php
                                if ($subcategory->allow_non_cash) {
                                    echo "<td>Non Cash</td>";
                                } else {
                                    echo "<td>Cash</td>";
                                }

                                if ($subcategory->allow_unit) {
                                    echo "<td>Unit</td>";
                                } else {
                                    echo "<td>Non Unit</td>";
                                }
                                ?>
                                <td>{{ $subcategory->type_of_trx }}</td>
                                <td>{{ $subcategory->categorytrx_uuid }}</td>
                                <td>{{ $subcategory->last_usage }}</td>
                                <td>
                                    <form method="post" action="{!! url('/subcategory/edit'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $subcategory->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/subcategory/hapus'); !!}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $subcategory->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>