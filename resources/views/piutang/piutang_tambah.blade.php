<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
        <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
        <title>TemanBisnis - Piutang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Piutang - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/piutang'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/piutang/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Tanggal Pencatatan</label>
                            <input type="text" name="add_date" class="form-control" placeholder="Tanggal Pencatatan ..">
                            @if($errors->has('add_date'))
                                <div class="text-danger">
                                    {{ $errors->first('add_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tanggal Jatuh Tempo</label>
                            <input type="text" name="due_date" class="form-control" placeholder="Tanggal Jatuh Tempo ..">
                             @if($errors->has('due_date'))
                                <div class="text-danger">
                                    {{ $errors->first('due_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="category" class="form-control">
                                <option value="Utang">Utang</option>
                                <option value="Piutang">Piutang</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Nama Pengguna</label>
                            <select id="select" name="select" class="form-control">
                                @foreach($profile as $key=>$p)
                                    <option value="{{ $p->uuid }}">{{ $p->owner_name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="profile_uuid" id="profile_uuid">
                            <p>Last selected: <code id="last-selected"></code></p>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('#select')
                .editableSelect()
                .on('select.editable-select', function (e, li){
                    $('#profile_uuid').val(li.attr('value')); 
                    li.value = li.attr('value')
                    $('#last-selected').html(
                        li.attr('value') + '. ' + li.text() + " | " + li.val() + '. ' + li.text() 
                    );
                });
        </script>
    </body>
</html>