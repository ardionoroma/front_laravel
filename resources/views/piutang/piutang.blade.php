<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .btn-link {
                border: none;
                outline: none;
                background: none;
                cursor: pointer;
                color: #0000EE;
                padding: 0;
                text-decoration: underline;
                font-family: inherit;
                font-size: inherit;
            }
        </style>
        <title>TemanBisnis - Piutang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <a href="{!! url('/'); !!}" style="float: left;"><h6>Beranda</h6></a>
                    <h6 style="float: right;">CRUD Data Piutang</h6>
                </div>
                <div class="card-body">
                    <div>
                        <a href="{!! url('/piutang/tambah'); !!}" class="btn btn-primary">Input Piutang Baru</a>
                        <form class="form-inline" method="post" action="{!! url('/piutang/search'); !!}" style="float: right;">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" name="search" class="form-control" placeholder="Cari ..">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Cari">
                            </div>
                        </form>
                    </div>
                    <br/>
                    <br/>
                    <table class="table table-bordered table-hover table-striped" style="width: 85%; table-layout: auto; border-collapse: collapse; margin: 0 auto; text-align: center;">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>ID Piutang</th>
                                <th>OPSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($piutang as $key=>$p)
                            <tr>
                                <td>{{ ++$key }}.</td>
                                <td>
                                    <form action="{!! url('/piutang/detail'); !!}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" name="detail" value="{{ $p->uuid }}" class="btn-link">{{ $p->uuid }}</button>
                                    </form>
                                </td>
                                <td>
                                    <form method="post" action="{!! url('/piutang/edit'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-warning" value="Edit">
                                    </form>
                                    <form method="post" action="{!! url('/piutang/hapus'); !!}" style="display: inline-block;">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="uuid" value="{{ $p->uuid }}">
                                        <input type="submit" class="btn btn-danger" value="Hapus">
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>