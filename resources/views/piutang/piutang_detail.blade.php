<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Piutang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Piutang
                </div>
                <div class="card-body" style="text-align: center;">
                    <a href="{!! url('/piutang'); !!}" class="btn btn-primary" style="float: left">Kembali ke Halaman Utama</a>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <h1>Detail {{$category}}</h1>
                    <h3>ID: {{$piutang->uuid}}</h3>
                    <br/>
                    <table class="table table-hover table-striped" style="width: 75%; table-layout: fixed; margin: 0 auto">
                        <tr>
                            <th>Tanggal Pencatatan</th>
                            <td>{{ $piutang->add_date }}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Jatuh Tempo</th>
                            <td>{{ $piutang->due_date }}</td>
                        </tr>
                        <tr>
                            <th>Kategori</th>
                            <td>{{ $piutang->category }}</td>
                        </tr>
                        <tr>
                            <th>ID Pengguna</th>
                            <td>{{ $piutang->profile_uuid }}</td>
                        </tr>
                        <tr>
                            <td>
                                <form method="post" action="{!! url('/piutang/edit'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $piutang->uuid }}">
                                    <input type="submit" class="btn btn-warning" value="Edit" style="float: right">
                                </form>
                            </td>
                            <td>
                                <form method="post" action="{!! url('/piutang/hapus'); !!}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="uuid" value="{{ $piutang->uuid }}">
                                    <input type="submit" class="btn btn-danger" value="Hapus" style="float: left">
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>