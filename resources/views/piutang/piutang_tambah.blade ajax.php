<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            input[type=text] {
                border: 2px solid #bdbdbd;
                font-family: 'Roboto', Arial, Sans-serif;
                font-size: 15px;
                font-weight: 400;
                padding: .5em .75em;
            }
            input[type=text]:focus {
                border: 2px solid #757575;
                outline: none;
            }
            .autocomplete-suggestions {
                border: 1px solid #999;
                background: #FFF;
                overflow: auto;
            }
            .autocomplete-suggestion {
                padding: 2px 5px;
                white-space: nowrap;
                overflow: hidden;
            }
            .autocomplete-selected {
                background: #F0F0F0;
            }
            .autocomplete-suggestions strong {
                font-weight: normal;
                color: #3399FF;
            }
            .autocomplete-group {
                padding: 2px 5px;
            }
            .autocomplete-group strong {
                display: block;
                border-bottom: 1px solid #000;
            }
        </style>
        <!-- Memanggil jQuery.js -->
        <script src="{{asset('js/jquery-3.4.1.js')}}"></script>

        <!-- Memanggil Autocomplete.js -->
        <script src="{{asset('js/jquery.autocomplete.js')}}"></script>
        <title>TemanBisnis - Piutang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Piutang - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/piutang'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/piutang/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Tanggal Pencatatan</label>
                            <input type="text" name="add_date" class="form-control" placeholder="Tanggal Pencatatan ..">
                            @if($errors->has('add_date'))
                                <div class="text-danger">
                                    {{ $errors->first('add_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tanggal Jatuh Tempo</label>
                            <input type="text" name="due_date" class="form-control" placeholder="Tanggal Jatuh Tempo ..">
                             @if($errors->has('due_date'))
                                <div class="text-danger">
                                    {{ $errors->first('due_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="category" class="form-control">
                                <option value="Utang">Utang</option>
                                <option value="Piutang">Piutang</option>
                            </select>
                        </div>

                        <!-- <div class="form-group">
                            <label>ID Pengguna</label>
                            <input type="text" name="profile_uuid" class="form-control" placeholder="ID Pengguna ..">
                             @if($errors->has('profile_uuid'))
                                <div class="text-danger">
                                    {{ $errors->first('profile_uuid')}}
                                </div>
                            @endif
                        </div> -->

                        <div class="form-group">
                            <label>ID Pengguna</label>
                            <input type="text" id="buah" name="buah" class="form-control" placeholder="ID Pengguna .." value="">
                             @if($errors->has('profile_uuid'))
                                <div class="text-danger">
                                    {{ $errors->first('profile_uuid')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function() {
                // Selector input yang akan menampilkan autocomplete.
                $( "#buah" ).autocomplete({
                    serviceUrl: "{!! url('/piutang/cari'); !!}",   // Kode php untuk prosesing data.
                    dataType: "JSON",           // Tipe data JSON.
                    onSelect: function (suggestion) {
                        $( "#buah" ).val(suggestion.value);
                    }
                });
            })
        </script>
    </body>
</html>