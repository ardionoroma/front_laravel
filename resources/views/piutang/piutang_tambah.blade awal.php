<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>TemanBisnis - Piutang</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    CRUD Data Piutang - <strong>TAMBAH DATA</strong>
                </div>
                <div class="card-body">
                    <a href="{!! url('/piutang'); !!}" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>                    
                    <form method="post" action="{!! url('/piutang/store'); !!}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Tanggal Pencatatan</label>
                            <input type="text" name="add_date" class="form-control" placeholder="Tanggal Pencatatan ..">
                            @if($errors->has('add_date'))
                                <div class="text-danger">
                                    {{ $errors->first('add_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Tanggal Jatuh Tempo</label>
                            <input type="text" name="due_date" class="form-control" placeholder="Tanggal Jatuh Tempo ..">
                             @if($errors->has('due_date'))
                                <div class="text-danger">
                                    {{ $errors->first('due_date')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Kategori</label>
                            <select name="category" class="form-control">
                                <option value="Utang">Utang</option>
                                <option value="Piutang">Piutang</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>ID Pengguna</label>
                            <input type="text" name="profile_uuid" class="form-control" placeholder="ID Pengguna ..">
                             @if($errors->has('profile_uuid'))
                                <div class="text-danger">
                                    {{ $errors->first('profile_uuid')}}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>