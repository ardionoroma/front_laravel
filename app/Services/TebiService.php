<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class TebiService
{
	use ConsumesExternalService;

	public $baseUri;

	public function __construct(){
		$this->baseUri = config('services.tebi.base_uri');
	}

	public function getCategorytrx($authorization){
		return $this->performRequest('GET', '/tebi/categorytrx', null, $authorization);
	}

	public function storeCategorytrx($data, $authorization){
		return $this->performRequest('POST', '/tebi/categorytrx', $data, $authorization);
	}

	public function findCategorytrx($data, $authorization){
		return $this->performRequest('GET', '/tebi/categorytrx/find', $data, $authorization);
	}

	public function showCategorytrx($data, $authorization){
		return $this->performRequest('GET', '/tebi/categorytrx/detail', $data, $authorization);
	}

	public function updateCategorytrx($data, $authorization){
		return $this->performRequest('PUT', '/tebi/categorytrx', $data, $authorization);
	}

	public function destroyCategorytrx($data, $authorization){
		return $this->performRequest('DELETE', '/tebi/categorytrx', $data, $authorization);
	}

	public function getCustomersupplier($authorization){
		return $this->performRequest('GET', '/tebi/cs', null, $authorization);
	}

	public function storeCustomersupplier($data, $authorization){
		return $this->performRequest('POST', '/tebi/cs', $data, $authorization);
	}

	public function findCustomersupplier($data, $authorization){
		return $this->performRequest('GET', '/tebi/cs/find', $data, $authorization);
	}

	public function showCustomersupplier($data, $authorization){
		return $this->performRequest('GET', '/tebi/cs/detail', $data, $authorization);
	}

	public function updateCustomersupplier($data, $authorization){
		return $this->performRequest('PUT', '/tebi/cs', $data, $authorization);
	}

	public function destroyCustomersupplier($data, $authorization){
		return $this->performRequest('DELETE', '/tebi/cs', $data, $authorization);
	}

	public function getTransaction($authorization){
		return $this->performRequest('GET', '/tebi/transaction', null, $authorization);
	}

	public function storeTransaction($data, $authorization){
		return $this->performRequest('POST', '/tebi/transaction', $data, $authorization);
	}

	public function findTransaction($data, $authorization){
		return $this->performRequest('GET', '/tebi/transaction/find', $data, $authorization);
	}

	public function showTransaction($data, $authorization){
		return $this->performRequest('GET', '/tebi/transaction/detail', $data, $authorization);
	}

	public function updateTransaction($data, $authorization){
		return $this->performRequest('PUT', '/tebi/transaction', $data, $authorization);
	}

	public function destroyTransaction($data, $authorization){
		return $this->performRequest('DELETE', '/tebi/transaction', $data, $authorization);
	}

	// public function batchTransaction($data, $authorization){
	// 	return $this->performRequest('DELETE', '/tebi/transaction/batch', $data, $authorization);
	// }

	public function getStock($authorization){
		return $this->performRequest('GET', '/tebi/stock', null, $authorization);
	}

	public function storeStock($data, $authorization){
		return $this->performRequest('POST', '/tebi/stock', $data, $authorization);
	}

	public function findStock($data, $authorization){
		return $this->performRequest('GET', '/tebi/stock/find', $data, $authorization);
	}

	public function showStock($data, $authorization){
		return $this->performRequest('GET', '/tebi/stock/detail', $data, $authorization);
	}

	public function updateStock($data, $authorization){
		return $this->performRequest('PUT', '/tebi/stock', $data, $authorization);
	}

	public function destroyStock($data, $authorization){
		return $this->performRequest('DELETE', '/tebi/stock', $data, $authorization);
	}

	public function getStockmutation($authorization){
		return $this->performRequest('GET', '/tebi/sales', null, $authorization);
	}

	public function storeStockmutation($data, $authorization){
		return $this->performRequest('POST', '/tebi/sales', $data, $authorization);
	}

	public function findStockmutation($data, $authorization){
		return $this->performRequest('GET', '/tebi/sales/find', $data, $authorization);
	}

	public function showStockmutation($data, $authorization){
		return $this->performRequest('GET', '/tebi/sales/detail', $data, $authorization);
	}

	public function updateStockmutation($data, $authorization){
		return $this->performRequest('PUT', '/tebi/sales', $data, $authorization);
	}

	public function destroyStockmutation($data, $authorization){
		return $this->performRequest('DELETE', '/tebi/sales', $data, $authorization);
	}

	public function firstLogin($data){
		return $this->first('POST', '/tebi/login', $data);
	}

	public function register($data){
		return $this->first('POST', '/tebi/register', $data);
	}

	public function getUser($authorization){
		return $this->performRequest('GET', '/tebi/user', null, $authorization);
	}

	public function editUser($data, $authorization){
		return $this->performRequest('PUT', '/tebi/edit', $data, $authorization);
	}

	public function logout($authorization){
		return $this->performRequest('POST', '/tebi/logout', null, $authorization);
	}
}