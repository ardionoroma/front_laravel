<?php

namespace App\Libraries;

class RupiahFormatter {
	public function getRupiah($nominal){
		$hasil = "Rp " . number_format($nominal,2,',','.');
		return $hasil;
	}
}