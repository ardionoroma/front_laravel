<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SubcategoryController extends Controller
{
    public function index(){
    	$client = new \GuzzleHttp\Client();
    	$request = $client->get('http://34.87.9.208/tebi/api/subcategory');
    	$subcategory = json_decode($request->getBody()->getContents());
    	return view('subcategory.subcategory', ['subcategory' => $subcategory->content]);
    }

    public function detail(Request $request){
        $subcategory = $this->process($request->detail);
        
        return view('subcategory.subcategory_detail', ['subcategory' => $subcategory->content]);
    }

    public function search(Request $request){
        $subcategory = $this->process($request->search);
        return view('subcategory.subcategory_cari', ['source' => $subcategory->source, 'subcategory' => $subcategory->content]);
    }

    public function tambah(){
    	return view('subcategory.subcategory_tambah');
    }

    /* Save new recorded transaction to database. */
    public function store(Request $request){
    	$this->validate($request, [
            'title' => 'required',
            'image_url' => 'required|url',
            'allow_non_cash' => 'required|boolean',
            'allow_unit' => 'required|boolean',
            'type_of_trx' => 'required',
            'categorytrx_uuid' => 'required'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'title' => $request->title,
            'image_url' => $request->image_url,
            'allow_non_cash' => $request->allow_non_cash,
            'allow_unit' => $request->allow_unit,
            'type_of_trx' => $request->type_of_trx,
            'categorytrx_uuid' => $request->categorytrx_uuid
        ];

        $response = $client->post(
            "/tebi/api/subcategory", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        return redirect('/subcategory');
    }

    public function edit(Request $request){
        $subcategory = $this->process($request->uuid);
    	return view('subcategory.subcategory_edit', ['subcategory' => $subcategory->content]);
    }

    public function update(Request $request){
    	$this->validate($request, [
            'title' => 'required',
            'image_url' => 'required|url',
            'allow_non_cash' => 'required|boolean',
            'allow_unit' => 'required|boolean'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        // $options = [
        //     'nominal' => $request->nominal,
        //     'note' => $request->note,
        //     'piutang_uuid' => $request->piutangId,
        //     'customersupplier_uuid' => $request->csId,
        //     'categorytrx_uuid' => $request->catId,
        //     'stockmutation_uuid' => $request->smId
        // ];

        $requestContent = [
            'uuid' => $request->uuid,
            // 'content' => $options
            'title' => $request->title,
            'image_url' => $request->image_url,
            'allow_non_cash' => $request->allow_non_cash,
            'allow_unit' => $request->allow_unit
        ];

        $response = $client->put(
            "/tebi/api/subcategorysatu", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        \Log::info($requestContent);

        return redirect('/subcategory');
    }

    public function delete(Request $request){
    	$client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'uuid' => $request->uuid
        ];

    	$request = $client->delete(
            "/tebi/api/subcategory", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
    	return redirect('/subcategory');
    }

    public function process($id){
        \Log::info($id);
        $client = new \GuzzleHttp\Client([
            "base_uri" => "http://34.87.9.208",
            "defaults" => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);

        $requestContent = [
            'id' => $id
        ];

        $request = $client->post(
            "/tebi/api/subcategorysearch", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
        return json_decode($request->getBody()->getContents());
    }
}
