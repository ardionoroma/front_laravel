<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function index(){
    	$client = new \GuzzleHttp\Client();
    	$request = $client->get('http://34.87.9.208/tebi/api/note');
    	$note = json_decode($request->getBody()->getContents());
    	return view('note.note', ['note' => $note->content]);
    }

    public function search(Request $request){
        $note = $this->process($request->search);
        return view('note.note_cari', ['source' => $note->source, 'note' => $note->content]);
    }

    public function tambah(){
    	return view('note.note_tambah');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'profile_uuid' => 'required'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'title' => $request->title,
            'content' => $request->content,
            'profile_uuid' => $request->profile_uuid
        ];

        $response = $client->post(
            "/tebi/api/note", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        return redirect('/note');
    }

    public function edit(Request $request){
        $note = $this->process($request->uuid);
    	return view('note.note_edit', ['note' => $note->content]);
    }

    public function update(Request $request){

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        // $options = [
        //     'nominal' => $request->nominal,
        //     'note' => $request->note,
        //     'piutang_uuid' => $request->piutangId,
        //     'customersupplier_uuid' => $request->csId,
        //     'categorytrx_uuid' => $request->catId,
        //     'stockmutation_uuid' => $request->smId
        // ];

        $requestContent = [
        	'uuid' => $request->uuid,
            'title' => $request->title,
            'content' => $request->content
        ];

        $response = $client->put(
            "/tebi/api/notesatu", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        \Log::info($requestContent);

        return redirect('/note');
    }

    public function delete(Request $request){
    	$client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'uuid' => $request->uuid
        ];

    	$request = $client->delete(
            "/tebi/api/note", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
    	return redirect('/note');
    }

    public function process($id){
        \Log::info($id);
        $client = new \GuzzleHttp\Client([
            "base_uri" => "http://34.87.9.208",
            "defaults" => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);

        $requestContent = [
            'id' => $id
        ];

        $request = $client->post(
            "/tebi/api/notesearch", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
        return json_decode($request->getBody()->getContents());
    }
}
