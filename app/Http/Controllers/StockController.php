<?php

namespace App\Http\Controllers;

use App\Libraries\RupiahFormatter;
use App\Services\TebiService;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    public function index(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $stock = json_decode($this->tebiService->getStock($authorization));

    	return view('stock.stock', ['stock' => $stock->content]);
    }

    public function add(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        return view('stock.stock_tambah');
    }

    public function store(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'name' => 'required|max:100',
            'image_url' => 'max:255|url|nullable',
            'purc_price' => 'required|integer|min:1',
            'sell_price' => 'required|integer|min:1',
            'qty' => 'required|integer|min:1',
            'type' => ['required', 'regex:~(?i)^pcs$|^box$|^lusin$|^dus$|^ton$|^kg$|^ltr$|^m$|^unit$|^gr$|^cm$|^order$~'],
        ]);

        if (empty($request->image_url)) {
            $request->image_url = "";
        }

        $data = [
        	'name' => $request->name,
        	'description' => $request->description,
            'image_url' => $request->image_url,
            'purc_price' => $request->purc_price,
            'sell_price' => $request->sell_price,
            'qty' => $request->qty,
            'type' => $request->type,
        ];

    	if ($this->tebiService->storeStock($data, $authorization)) {
            return redirect('/stock');
        }
    }

    public function find(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'cari' => $request->cari,
        ];

        $stock = json_decode($this->tebiService->findStock($data, $authorization));

        return view('stock.stock_cari', ['source' => $stock->source, 'stock' => $stock->content]);
    }

    public function show(Request $request){
        $rupiah = new RupiahFormatter();

        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $stock = $this->process($request->id, $authorization);
        
        return view('stock.stock_detail', ['stock' => $stock->content, 'purc_price' => $rupiah->getRupiah($stock->content->purc_price), 'sell_price' => $rupiah->getRupiah($stock->content->sell_price)]);
    }

    public function edit(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $stock = $this->process($request->id, $authorization);

    	return view('stock.stock_edit', ['stock' => $stock->content]);
    }

    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'name' => 'required|max:100',
            'image_url' => 'max:255|url|nullable',
            'purc_price' => 'required|integer|min:1',
            'sell_price' => 'required|integer|min:1',
            'qty' => 'required|integer|min:0',
        ]);

        if (empty($request->image_url)) {
            $request->image_url = "";
        }

        $data = [
        	'id' => $request->id,
            'name' => $request->name,
        	'description' => $request->description,
            'image_url' => $request->image_url,
            'purc_price' => $request->purc_price,
            'sell_price' => $request->sell_price,
            'qty' => $request->qty,
        ];

        if ($this->tebiService->updateStock($data, $authorization)) {
            return redirect('/stock');
        }
    }

    public function destroy(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'id' => $request->id
        ];

        if ($this->tebiService->destroyStock($data, $authorization)) {
            return redirect('/stock');
        }
    }

    public function process($id, $authorization){
        $data = [
            'id' => $id,
        ];

        return json_decode($this->tebiService->showStock($data, $authorization));
    }
}
