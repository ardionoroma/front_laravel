<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PiutangController extends Controller
{
    /* Get all piutang. */
    public function index(){
    	$client = new \GuzzleHttp\Client();
    	$request = $client->get('http://34.87.9.208/tebi/api/piutang');
    	$piutang = json_decode($request->getBody()->getContents());
    	return view('piutang.piutang', ['piutang' => $piutang->content]);
    }

    public function detail(Request $request){
    	$piutang = $this->process($request->detail);
    	$cat = "N/A";
    	if (substr($request->detail, 2, 3) == "PTG") {
    		$cat = "Piutang";
    	} else if (substr($request->detail, 2, 3) == "UTG") {
    		$cat = "Utang";
    	}
        return view('piutang.piutang_detail', ['piutang' => $piutang->content, 'category' => $cat]);
    }

    /*  Get piutang with params, it can be:
        1. Category: "Piutang"/"Utang"
        2. Date Range for add_date: "masuk yyyy-mm-dd sd yyyy-mm-dd"
        3. Date Range for due_date: "tempo yyyy-mm-dd sd yyyy-mm-dd"
        4. Specific Date for add_date: "masuk yyyy-mm-dd"
        5. Specific Date for due_date: "tempo yyyy-mm-dd"
        6. User ID
        7. Piutang ID
    */
    public function search(Request $request){
        $piutang = $this->process($request->search);
        return view('piutang.piutang_cari', ['source' => $piutang->source, 'piutang' => $piutang->content]);
    }

    /* Open Tambah Transaksi page. */
    public function tambah(){
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://34.87.9.208/tebi/api/profile');
        $profile = json_decode($request->getBody()->getContents());
    	return view('piutang.piutang_tambah', ['profile' => $profile->content]);
    }

    // public function cari(){
    //     $client = new \GuzzleHttp\Client();
    //     $request = $client->get('http://34.87.9.208/tebi/api/profile');
    //     $profile = json_decode($request->getBody()->getContents());
    //     foreach ($profile->content as $value) {
    //         $output['suggestions'][] = [
    //             'value' => $value->uuid,
    //             'buah'  => $value->owner_name
    //         ];
    //         // $x[$value->uuid] = $value->owner_name;
    //         // echo $value->uuid." | ".$value->owner_name."<br>";
    //     }
    //     return json_encode($output);
    //     // print_r($profile->content);
    //     // return view('piutang.piutang', ['profile' => $profile->content]);
    // }

    /* Save new recorded transaction to database. */
    public function store(Request $request){
        \Log::info($request->profile_uuid);
    	$this->validate($request, [
            'add_date' => 'required|date',
            'due_date' => 'required|date',
            'category' => 'required',
            'profile_uuid' => 'required'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'add_date' => $request->add_date,
            'due_date' => $request->due_date,
            'category' => $request->category,
            'profile_uuid' => $request->profile_uuid
        ];

        $response = $client->post(
            "/tebi/api/piutang", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        return redirect('/piutang');
    }

    /* Open Edit Transaction page. It uses Process function because it will search the corresponding transaction first. */
    public function edit(Request $request){
        $piutang = $this->process($request->uuid);
    	return view('piutang.piutang_edit', ['piutang' => $piutang->content]);
    }

    /* Update transaction. */
    public function update(Request $request){
    	$this->validate($request, [
            'add_date' => 'required|date',
            'due_date' => 'required|date'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        // $options = [
        //     'add_date' => $request->add_date,
        //     'due_date' => $request->due_date
        // ];

        $requestContent = [
            'uuid' => $request->uuid,
            // 'content' => $options
            'add_date' => $request->add_date,
            'due_date' => $request->due_date
        ];

        $response = $client->put(
            "/tebi/api/piutangsatu", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        \Log::info($requestContent);

        return redirect('/piutang');
    }

    /* Delete transaction. */
    public function delete(Request $request){
    	$client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'uuid' => $request->uuid
        ];

    	$request = $client->delete(
            "/tebi/api/piutang", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
    	return redirect('/piutang');
    }

    /* Function for searching specific transactions in database. */
    public function process($id){
        \Log::info($id);
        $client = new \GuzzleHttp\Client([
            "base_uri" => "http://34.87.9.208",
            "defaults" => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);

        $requestContent = [
            'id' => $id
        ];

        $request = $client->post(
            "/tebi/api/piutangsearch", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
        return json_decode($request->getBody()->getContents());
    }
}
