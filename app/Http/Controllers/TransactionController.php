<?php

namespace App\Http\Controllers;

use App\Libraries\RupiahFormatter;
use App\Services\TebiService;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    /* Get all transactions. */
    public function index(Request $request){
        $rupiah = new RupiahFormatter();
    	// $transaksi = $this->all("06PFL20190813TEBI0E8W5J0000042", null);
     //    return view('transaksi.transaksi', ['bulan' => $transaksi->month, 'transaksi' => $transaksi->content, 'count' => 0]);
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $transaction = json_decode($this->tebiService->getTransaction($authorization));

        return view('transaction.transaction', ['transaction' => $transaction->content, 'balance' => $rupiah->getRupiah($transaction->balance), 'count' => 0]);
    }

    /* Open Tambah Transaksi page. */
    public function add(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $categorytrx = json_decode($this->tebiService->getCategorytrx($authorization));
        $cs = json_decode($this->tebiService->getCustomersupplier($authorization));
        
        return view('transaction.transaction_tambah', ['categorytrx' => $categorytrx->content, 'cs' => $cs->content]);
    }

    /* Save new recorded transaction to database. */
    public function store(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $this->validate($request, [
            'nominal' => 'required|integer|min:1',
            'date' => 'required|date|date_format:Y-m-d',
            'cat_id' => 'required|integer',
            'cs_id' => 'integer|nullable'
        ]);

        if (empty($request->cs_id) || $request->cs_id == "0") {
            $request->cs_id = "";
        }

        $data = [
            'nominal' => $request->nominal,
            'note' => $request->note,
            'date' => $request->date,
            'cat_id' => $request->cat_id,
            'cs_id' => $request->cs_id
        ];

        if ($this->tebiService->storeTransaction($data, $authorization)) {
            return redirect('/transaction');
        }
    }

    public function find(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'cari' => $request->cari,
        ];

        $transaction = json_decode($this->tebiService->findTransaction($data, $authorization));
        
        return view('transaction.transaction_cari', ['source' => $transaction->source, 'transaction' => $transaction->content, 'count' => 0]);
    }

    public function show(Request $request){
        $rupiah = new RupiahFormatter();

        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $transaction = $this->process($request->id, $authorization);

        return view('transaction.transaction_detail', ['transaction' => $transaction->content, 'nominal' => $rupiah->getRupiah($transaction->content->nominal)]);
    }

    /* Open Edit Transaction page. It uses Process function because it will search the corresponding transaction first. */
    public function edit(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $transaction = $this->process($request->id, $authorization);
        $categorytrx = json_decode($this->tebiService->getCategorytrx($authorization));
        $cs = json_decode($this->tebiService->getCustomersupplier($authorization));

    	return view('transaction.transaction_edit', ['transaction' => $transaction->content, 'categorytrx' => $categorytrx->content, 'cs' => $cs->content]);
    }

    /* Update transaction. */
    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'nominal' => 'required|integer|min:1',
            'date' => 'required|date|date_format:Y-m-d',
            'cat_id' => 'required|integer',
            'cs_id' => 'integer|nullable'
        ]);

        if (empty($request->cs_id) || $request->cs_id == "0") {
            $request->cs_id = "";
        }

        $data = [
            'id' => $request->id,
            'nominal' => $request->nominal,
            'note' => $request->note,
            'date' => $request->date,
            'cat_id' => $request->cat_id,
            'cs_id' => $request->cs_id
        ];

        if ($this->tebiService->updateTransaction($data, $authorization)) {
            return redirect('/transaction');
        }
    }

    /* Delete transaction. */
    public function destroy(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'id' => $request->id
        ];

    	if ($this->tebiService->destroyTransaction($data, $authorization)) {
            return redirect('/transaction');
        }
    }

    public function process($id, $authorization){
        $data = [
            'id' => $id,
        ];

        return json_decode($this->tebiService->showTransaction($data, $authorization));
    }
}
