<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
    	$client = new \GuzzleHttp\Client();
    	$request = $client->get('http://34.87.9.208/tebi/api/profile');
    	$profile = json_decode($request->getBody()->getContents());
    	return view('profile.profile', ['profile' => $profile->content]);
    }

    public function detail(Request $request){
    	$profile = $this->process($request->detail);
    	
        return view('profile.profile_detail', ['profile' => $profile->content]);
    }

    public function search(Request $request){
        $profile = $this->process($request->search);
        return view('profile.profile_cari', ['source' => $profile->source, 'profile' => $profile->content]);
    }

    public function tambah(){
    	return view('profile.profile_tambah');
    }

    public function store(Request $request){
    	$this->validate($request, [
            'email' => 'required|email',
            'business_name' => 'required',
            'business_category' => 'required',
            'business_model' => 'required'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'bank_account' => $request->bank_account,
            'owner_name' => $request->owner_name,
            'business_name' => $request->business_name,
            'business_category' => $request->business_category,
            'business_model' => $request->business_model,
            'fcm_token' => $request->fcm_token,
            'image_profile' => $request->image_profile,
            'website' => $request->website,
            'other_website' => $request->other_website,
            'city' => $request->city,
            'featured_product' => $request->featured_product,
            'fulfilled' => $request->fulfilled
        ];

        $response = $client->post(
            "/tebi/api/profile", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        return redirect('/profile');
    }

    public function edit(Request $request){
        $profile = $this->process($request->uuid);
    	return view('profile.profile_edit', ['profile' => $profile->content]);
    }

    public function update(Request $request){
    	$this->validate($request, [
            'business_name' => 'required',
            'business_category' => 'required',
            'business_model' => 'required'
        ]);

        $client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        // $options = [
        //     'add_date' => $request->add_date,
        //     'due_date' => $request->due_date
        // ];

        $requestContent = [
            'uuid' => $request->uuid,
            // 'content' => $options
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'bank_account' => $request->bank_account,
            'owner_name' => $request->owner_name,
            'business_name' => $request->business_name,
            'business_category' => $request->business_category,
            'business_model' => $request->business_model,
            'fcm_token' => $request->fcm_token,
            'image_profile' => $request->image_profile,
            'website' => $request->website,
            'other_website' => $request->other_website,
            'city' => $request->city,
            'featured_product' => $request->featured_product,
            'fulfilled' => $request->fulfilled
        ];

        $response = $client->put(
            "/tebi/api/profilesatu", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );

        \Log::info($requestContent);

        return redirect('/profile');
    }

    public function delete(Request $request){
    	$client = new \GuzzleHttp\Client(["base_uri" => "http://34.87.9.208"]);

        $requestContent = [
            'uuid' => $request->uuid
        ];

    	$request = $client->delete(
            "/tebi/api/profile", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
    	return redirect('/profile');
    }

    public function process($id){
        \Log::info($id);
        $client = new \GuzzleHttp\Client([
            "base_uri" => "http://34.87.9.208",
            "defaults" => [
                'headers' => ['content-type' => 'application/json']
            ]
        ]);

        $requestContent = [
            'id' => $id
        ];

        $request = $client->post(
            "/tebi/api/profilesearch", [
                \GuzzleHttp\RequestOptions::JSON => $requestContent
            ]
        );
        return json_decode($request->getBody()->getContents());
    }
}
