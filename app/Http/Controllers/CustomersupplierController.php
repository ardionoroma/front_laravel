<?php

namespace App\Http\Controllers;

use App\Services\TebiService;
use Illuminate\Http\Request;

class CustomersupplierController extends Controller
{
    public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    public function index(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $cs = json_decode($this->tebiService->getCustomersupplier($authorization));

    	return view('cs.cs', ['cs' => $cs->content]);
    }

    public function add(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        return view('cs.cs_tambah');
    }

    public function store(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $this->validate($request, [
            'name' => 'required|max:100',
            'address' => 'max:250',
            'phone' => 'max:25',
            'email' => 'email|max:100|nullable',
            'bank_account' => 'max:50',
            'category' => ['required', 'regex:~(?i)^customer$|^supplier$~'], //bikin lemot
        ]);

        if (empty($request->email)) {
            $request->email = "";
        }

        $data = [
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'email' => $request->email,
            'bank_account' => $request->bank_account,
            'category' => $request->category,
        ];

        if ($this->tebiService->storeCustomersupplier($data, $authorization)) {
            return redirect('/cs');
        }
    }

    public function find(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'cari' => $request->cari,
        ];

        $cs = json_decode($this->tebiService->findCustomersupplier($data, $authorization));

        return view('cs.cs_cari', ['source' => $cs->source, 'cs' => $cs->content]);
    }

    public function show(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $cs = $this->process($request->id, $authorization);
        
        return view('cs.cs_detail', ['cs' => $cs->content]);
    }

    public function edit(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $cs = $this->process($request->id, $authorization);

    	return view('cs.cs_edit', ['cs' => $cs->content]);
    }

    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'name' => 'required|max:100',
            'address' => 'max:250',
            'phone' => 'max:25',
            'email' => 'email|max:100|nullable',
            'bank_account' => 'max:50',
        ]);

        if (empty($request->email)) {
            $request->email = "";
        }

        $data = [
        	'id' => $request->id,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'email' => $request->email,
            'bank_account' => $request->bank_account
        ];

        if ($this->tebiService->updateCustomersupplier($data, $authorization)) {
            return redirect('/cs');
        }
    }

    public function destroy(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'id' => $request->id
        ];

        if ($this->tebiService->destroyCustomersupplier($data, $authorization)) {
            return redirect('/cs');
        }
    }

    public function process($id, $authorization){
        $data = [
            'id' => $id,
        ];

        return json_decode($this->tebiService->showCustomersupplier($data, $authorization));
    }
}
