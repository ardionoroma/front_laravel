<?php

namespace App\Http\Controllers;

use App\Libraries\RupiahFormatter;
use App\Services\TebiService;
use Illuminate\Http\Request;

class StockmutationController extends Controller
{
    public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    public function index(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $sales = json_decode($this->tebiService->getStockmutation($authorization));

        return view('sales.sales', ['sales' => $sales->content, 'count' => 0]);
    }

    public function add(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $stock = json_decode($this->tebiService->getStock($authorization));
        $cs = json_decode($this->tebiService->getCustomersupplier($authorization));
        
        return view('sales.sales_tambah', ['stock' => $stock->content, 'cs' => $cs->content]);
    }

    public function store(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $this->validate($request, [
            'qty' => 'required|integer|min:1',
            'stk_id' => 'required|integer',
            'date' => 'required|date|date_format:Y-m-d',
            'cs_id' => 'integer',
        ]);

        $data = [
            'qty' => $request->qty * $request->category,
            'stk_id' => $request->stk_id,
            'date' => $request->date,
            'cs_id' => $request->cs_id,
        ];

        if ($this->tebiService->storeStockmutation($data, $authorization)) {
            return redirect('/sales');
        }
    }

    public function find(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'cari' => $request->cari,
        ];

        $sales = json_decode($this->tebiService->findStockmutation($data, $authorization));
        
        return view('sales.sales_cari', ['source' => $sales->source, 'sales' => $sales->content, 'count' => 0]);
    }

    public function show(Request $request){
        $rupiah = new RupiahFormatter();

        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $sales = $this->process($request->id, $authorization);

        // $data = [
        // 	'id' => $sales->content->trx_id
        // ];

        // $transaction = json_decode($this->tebiService->findTransaction($data));

        return view('sales.sales_detail', ['sales' => $sales->content, 'price' => $rupiah->getRupiah($sales->content->price)]);
    }

    public function edit(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $sales = $this->process($request->id, $authorization);
        $cs = json_decode($this->tebiService->getCustomersupplier($authorization));

    	return view('sales.sales_edit', ['sales' => $sales->content, 'cs' => $cs->content]);
    }

    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'qty' => 'required|integer|min:1',
            'date' => 'required|date|date_format:Y-m-d',
            'cs_id' => 'integer',
        ]);

        if (($request->qty_before < 0 && $request->qty >= 0) || ($request->qty_before >= 0 && $request->qty < 0)) {
            $request->qty *= -1;
        }

        $data = [
            'id' => $request->id,
            'qty' => $request->qty,
            'note' => $request->note,
            'date' => $request->date,
            'cs_id' => $request->cs_id,
        ];

        if ($this->tebiService->updateStockmutation($data, $authorization)) {
            return redirect('/sales');
        }
    }

    public function destroy(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'id' => $request->id
        ];

    	if ($this->tebiService->destroyStockmutation($data, $authorization)) {
            return redirect('/sales');
        }
    }

    public function process($id, $authorization){
        $data = [
            'id' => $id,
        ];

        return json_decode($this->tebiService->showStockmutation($data, $authorization));
    }
}
