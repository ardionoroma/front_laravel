<?php

namespace App\Http\Controllers;

use App\Services\TebiService;
use Exception;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class UserController extends Controller
{
	public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    public function index(Request $request){
        $token = $request->cookie('token');
        return view('welcome', ['token' => $token]);
    }

	public function login(Request $request){
        if ($request->cookie('token')) {
            return redirect('/');
        }
		return view('user.login');
	}

	public function auth(Request $request){
		$this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        try {
        	$x = json_decode($this->tebiService->firstLogin($data));
        } catch (Exception $e) {
        	if ($e instanceof ClientException) {
	            $message = $e->getResponse()->getBody()->getContents();

	            if (strpos($message, 'invalid_credentials') !== false) {
	            	$invalid_credentials = true;
	            	return view('user.login', compact('invalid_credentials'));
	            }
	        }
        }
        
        Cookie::queue('token', $x->token, 60);
        return redirect('/');
	}

    public function register(Request $request){
        if ($request->cookie('token')) {
            return redirect('/');
        }
        return view('user.register');
    }

    public function store(Request $request){
        $this->validate($request, [
            'email' => 'required|string|max:100|email',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'max:255',
            'address' => 'max:255',
            'bank_account' => 'max:255',
            'owner_name' => 'max:255',
            'business_name' => 'required|max:100',
            'business_category' => 'required|max:100',
            'business_model' => ['required', 'regex:~(?i)\bproduksi\b|\bretail\b|\bjasa\b~'],
            'website' => 'max:255',
            'other_website' => 'max:255',
            'city' => 'max:255',
            'featured_product' => 'max:255'
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            'phone' => $request->phone,
            'address' => $request->address,
            'bank_account' => $request->bank_account,
            'owner_name' => $request->owner_name,
            'business_name' => $request->business_name,
            'business_category' => $request->business_category,
            'business_model' => $request->business_model,
            'website' => $request->website,
            'other_website' => $request->other_website,
            'city' => $request->city,
            'featured_product' => $request->featured_product,
        ];

        $x = json_decode($this->tebiService->register($data));
        
        Cookie::queue('token', $x->token, 60);
        return redirect('/');
    }

    public function user(Request $request){
        if (!$request->cookie('token')) {
            return redirect('/');
        }

        $token = $request->cookie('token');
        $user = json_decode($this->tebiService->getUser($token));
        return view('user.user', ['user' => $user->user]);
    }

    public function edit(Request $request){
        if (!$request->cookie('token')) {
            return redirect('/');
        }

        $token = $request->cookie('token');
        $user = json_decode($this->tebiService->getUser($token));
        return view('user.edit', ['user' => $user->user]);
    }

    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $this->validate($request, [
            'phone' => 'max:255',
            'address' => 'max:255',
            'bank_account' => 'max:255',
            'owner_name' => 'max:255',
            'business_name' => 'required|max:100',
            'business_category' => 'required|max:100',
            'business_model' => ['required', 'regex:~(?i)\bproduksi\b|\bretail\b|\bjasa\b~'],
            'website' => 'max:255',
            'other_website' => 'max:255',
            'city' => 'max:255',
            'featured_product' => 'max:255'
        ]);

        $data = [
            'phone' => $request->phone,
            'address' => $request->address,
            'bank_account' => $request->bank_account,
            'owner_name' => $request->owner_name,
            'business_name' => $request->business_name,
            'business_category' => $request->business_category,
            'business_model' => $request->business_model,
            'website' => $request->website,
            'other_website' => $request->other_website,
            'city' => $request->city,
            'featured_product' => $request->featured_product,
        ];

        if($this->tebiService->editUser($data, $authorization)){
            return redirect('/user');
        }
    }

    public function logout(Request $request){
        $authorization = $request->cookie('token');
        if ($this->tebiService->logout($authorization)) {
            Cookie::queue(Cookie::forget('token'));
        }

        return redirect('/');
    }

	public function getCk(Request $request){
		echo $request->cookie('token');
	}
}
