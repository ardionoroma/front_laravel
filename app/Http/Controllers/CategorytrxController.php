<?php

namespace App\Http\Controllers;

use App\Services\TebiService;
use Illuminate\Http\Request;

class CategorytrxController extends Controller
{
    public $tebiService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(TebiService $tebiService)
    {
        $this->tebiService = $tebiService;
    }

    public function index(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$categorytrx = json_decode($this->tebiService->getCategorytrx($authorization));

    	return view('categorytrx.categorytrx', ['categorytrx' => $categorytrx->content]);
    }

    public function add(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        return view('categorytrx.categorytrx_tambah');
    }

    /* Save new recorded transaction to database. */
    public function store(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $this->validate($request, [
            'title' => 'required|max:100',
            'image_url' => 'required|max:250|url',
            'allow_non_cash' => 'required|boolean',
            'allow_unit' => 'required|boolean',
            'type_of_trx' => ['required', 'regex:~(?i)^income$|^outcome$~'], //bikin lemot
        ]);

        $data = [
            'title' => $request->title,
            'image_url' => $request->image_url,
            'allow_non_cash' => $request->allow_non_cash,
            'allow_unit' => $request->allow_unit,
            'type_of_trx' => $request->type_of_trx,
        ];

        if ($this->tebiService->storeCategorytrx($data, $authorization)) {
            return redirect('/categorytrx');
        }
    }

    public function find(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'cari' => $request->cari,
        ];

        $categorytrx = json_decode($this->tebiService->findCategorytrx($data, $authorization));

        return view('categorytrx.categorytrx_cari', ['source' => $categorytrx->source, 'categorytrx' => $categorytrx->content]);
    }

    public function show(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $categorytrx = $this->process($request->id, $authorization);
        
        return view('categorytrx.categorytrx_detail', ['categorytrx' => $categorytrx->content]);
    }

    public function edit(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $categorytrx = $this->process($request->id, $authorization);

    	return view('categorytrx.categorytrx_edit', ['categorytrx' => $categorytrx->content]);
    }

    public function update(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

    	$this->validate($request, [
            'title' => 'required|max:100',
            'image_url' => 'required|max:250|url',
            'allow_non_cash' => 'required|boolean',
            'allow_unit' => 'required|boolean',
        ]);

        $data = [
            'id' => $request->id,
            'title' => $request->title,
            'image_url' => $request->image_url,
            'allow_non_cash' => $request->allow_non_cash,
            'allow_unit' => $request->allow_unit
        ];

        if ($this->tebiService->updateCategorytrx($data, $authorization)) {
            return redirect('/categorytrx');
        }
    }

    public function destroy(Request $request){
        $authorization = $request->cookie('token');
        if (empty($authorization)) {
            abort(401);
        }

        $data = [
            'id' => $request->id
        ];

    	if ($this->tebiService->destroyCategorytrx($data, $authorization)) {
            return redirect('/categorytrx');
        }
    }

    public function process($id, $authorization){
        $data = [
            'id' => $id,
        ];

        return json_decode($this->tebiService->showCategorytrx($data, $authorization));
    }
}
