<?php

namespace App\Traits;

use GuzzleHttp\Client;

trait ConsumesExternalService{

	/**
	 * Send a request to any service
	 * @return string
	 */
	public function performRequest($method, $requestUri, $body = [], $authorization){
		$client = new Client([
			'base_uri' => $this->baseUri,
		]);

		$response = $client->request($method, $requestUri, [
			'headers' => ['Authorization' => 'Bearer '.$authorization],
			\GuzzleHttp\RequestOptions::JSON => $body
		]);

		return $response->getBody()->getContents();
	}

	public function first($method, $requestUri, $body = []){
		$client = new Client([
			'base_uri' => $this->baseUri,
		]);

		$response = $client->request($method, $requestUri, [
			'headers' => ['Authorization' => 'req token'],
			\GuzzleHttp\RequestOptions::JSON => $body
		]);

		return $response->getBody()->getContents();
	}
}