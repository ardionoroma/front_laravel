<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* All parameters passed using Request instead of passing variable in route. */
Route::get('/categorytrx', 'CategorytrxController@index');
Route::get('/categorytrx/add', 'CategorytrxController@add');
Route::post('/categorytrx/store', 'CategorytrxController@store');
Route::get('/categorytrx/find', 'CategorytrxController@find');
Route::get('/categorytrx/detail', 'CategorytrxController@show');
Route::get('/categorytrx/edit', 'CategorytrxController@edit');
Route::put('/categorytrx/update', 'CategorytrxController@update');
Route::post('/categorytrx/destroy', 'CategorytrxController@destroy');

Route::get('/cs', 'CustomersupplierController@index');
Route::get('/cs/add', 'CustomersupplierController@add');
Route::post('/cs/store', 'CustomersupplierController@store');
Route::get('/cs/find', 'CustomersupplierController@find');
Route::get('/cs/detail', 'CustomersupplierController@show');
Route::get('/cs/edit', 'CustomersupplierController@edit');
Route::put('/cs/update', 'CustomersupplierController@update');
Route::post('/cs/destroy', 'CustomersupplierController@destroy');

Route::get('/transaction', 'TransactionController@index');
Route::get('/transaction/add', 'TransactionController@add');
Route::post('/transaction/store', 'TransactionController@store');
Route::get('/transaction/find', 'TransactionController@find');
Route::get('/transaction/detail', 'TransactionController@show');
Route::get('/transaction/edit', 'TransactionController@edit');
Route::put('/transaction/update', 'TransactionController@update');
Route::post('/transaction/destroy', 'TransactionController@destroy');

Route::get('/stock', 'StockController@index');
Route::get('/stock/add', 'StockController@add');
Route::post('/stock/store', 'StockController@store');
Route::get('/stock/find', 'StockController@find');
Route::get('/stock/detail', 'StockController@show');
Route::get('/stock/edit', 'StockController@edit');
Route::put('/stock/update', 'StockController@update');
Route::post('/stock/destroy', 'StockController@destroy');

Route::get('/sales', 'StockmutationController@index');
Route::get('/sales/add', 'StockmutationController@add');
Route::post('/sales/store', 'StockmutationController@store');
Route::get('/sales/find', 'StockmutationController@find');
Route::get('/sales/detail', 'StockmutationController@show');
Route::get('/sales/edit', 'StockmutationController@edit');
Route::put('/sales/update', 'StockmutationController@update');
Route::post('/sales/destroy', 'StockmutationController@destroy');

Route::get('/', 'UserController@index');
Route::get('/register', 'UserController@register');
Route::post('/store', 'UserController@store');
Route::get('/login', 'UserController@login');
Route::post('/auth', 'UserController@auth');
Route::get('/user', 'UserController@user');
Route::get('/edit', 'UserController@edit');
Route::put('/update', 'UserController@update');
Route::get('/logout', 'UserController@logout');
Route::get('/tes', 'UserController@getCk');

// Route::get('/note', 'NoteController@index');
// Route::get('/note/tambah', 'NoteController@tambah');
// Route::post('/note/store', 'NoteController@store');
// Route::post('/note/edit', 'NoteController@edit');
// Route::put('/note/update', 'NoteController@update');
// Route::post('/note/hapus', 'NoteController@delete');
// Route::post('/note/search/', 'NoteController@search');